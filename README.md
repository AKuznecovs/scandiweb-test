Scandiweb Junior Web Developer test 2.2
==============================

Author: Andrejs Kuznecovs;

Date: Sept 2020;

* * *

### Requirements
-   PHP: ​ 7.3;
-   MySQL: ​ 5.7.30;
-   Composer: 1.10.8
-   Node.js: 10.19.0 and NPM: 6.14.7 for using Gulp.js: 2.3.0

#### Servers:
-   Apache/2.4.4.1
-   NGINX 1.18.0

* * *

Setup
-------------

1. Config files for Nginx and Apache2 are located in `scandiweb-test/data` folder.

2. MYSQL dump file is located in `scandiweb-test/data/sql` folder.

3. Default configuration for DB (`scandiweb_test/config/config.php`):
    >User: root
    >
    >Password: root
    >
    >Database: scandiweb_test
* * *

Installation using bash scripts (Ubuntu)
-------------

1. Clone `scandiweb-test` project into `/var/www/` folder;

2. Go to project's `/data` folder;

3. Configure server using `apache2.sh` or `nginx.sh` scripts;

4. Configure MYSQL using `mysql.sh` script, change `config.php` file;

5. Install Composer Autoloader;

**If using Apache2:**

Run `apache2.sh` script for project's Apache2 configuration setup. 

This will create projects entry in `/etc/hosts`, will add/enable vhost with servername "swtest.local".
        
        sudo bash apache2.sh swtest
  
**If using NGINX:**
 
 Run `nginx.sh` script for project's NGINX configuration setup.
         
 This will create projects entry in `/etc/hosts`,
 will add/enable vhost with servername "swtest.local".

    sudo bash nginx.sh swtest  
    

**MYSQL setup**

Run the `mysql.sh` bash script. This will create user/database and will import table from the dump file for 
current project. Also this will create `credentials.txt` file with login information
in `/scandiweb-test/data/sql` folder.

        sudo bash mysql.sh
        

>User: admin
>
>Password: Option123#
>
>Database: scandiweb_test


#### Composer setup
Go to project's root folder `scandiweb-test/` and run:

        composer install


**After successful setup project should be accessible: `http://swtest.local`**


* * *

**Important!**

If both NGINX and Apache2 servers are running, set Apache2 ports to listen to :8080 in `/etc/apache2/ports.conf` file

Make changes to newly created `/etc/apache2/swtest.conf` file - set `<VirtualHost *:80>` to :8080 accordingly.

After successful change: `http://swtest.local:8080` will be available.

## Installation for other OS:
1. Clone `scandiweb-test` project into your webroot folder;
2. Project's main entry - `index.php` is located in `scandiweb-test/public` folder.
3. Go to project's `/data` folder. The MYSQL dump file is located
 in `scandiweb-test/data/sql`

* * *

## Usage
For public assets (SASS and jQuery) modification use Gulp.js.

Go to the project's root folder, run

    npm install

* * *

## About
This Junior Web Developer test 2.2 was created on Ubuntu 20.04 and was tested:

* using Ubuntu 20.04 Apache2 configuration;
* using Ubintu 20.04 NGINX configuration;

* * *

## License
[MIT](https://choosealicense.com/licenses/mit/)