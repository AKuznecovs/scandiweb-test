<?php

// Basic
define('APP_NAME', 'Simple PHP MVC Project Structure');
define('ROOT', realpath(dirname(__DIR__)));
define('SOURCE', ROOT . '/src');
define('HOST', 'http://' . $_SERVER['HTTP_HOST']);

// View
define('TEMPLATE_VIEW_PATH', SOURCE . '/Views/Templates/');
define('PAGE_VIEW_PATH', SOURCE . '/Views/Pages/');
define('PRODUCT_VIEW_PATH', SOURCE . '/Views/Products/');

//Assets
define('ASSETS_PATH', HOST . '/public/assets');
define('CSS_PATH', ASSETS_PATH . '/styles/css/main.css');
define('SCRIPT_PATH', ASSETS_PATH . '/scripts/script.js');
define('IMAGE_PATH', ASSETS_PATH . '/images/');

// Core
define('DEFAULT_CONTROLLER', 'main');
define('DEFAULT_CONTROLLER_ACTION', 'indexAction');
define('CONTROLLER_PATH', SOURCE . '/Controllers/');
define('MODEL_PATH', SOURCE . '/Models/');

// Database
define('DB_HOST', 'localhost');
define('DB_NAME', 'scandiweb_test');
define('DB_USER', 'admin');
define('DB_PASS', 'Option123#');
