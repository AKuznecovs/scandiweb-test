<?php
// Session:
session_start();

// Dev mode:
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once __DIR__ . '/../config/dev.php';

require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/../vendor/autoload.php';

use Core\Router;

$router = new Router();
$router->process();
