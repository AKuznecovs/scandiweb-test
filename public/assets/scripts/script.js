$(document).ready(function () {
    // Function redirects to product page.
    function redirect()
    {
        window.location.href = '/add';
    }

    // if no errors on page - hides error's <p> tags.
    $('.error:empty').remove();

    //Function submits form.
    function submitForm()
    {
        $('.main-form').submit();
    }

    //Function hides/shows product types and disables inputs if they are hidden.
    $('#switcher').change(function () {
        let $value      = this.value;
        let $cd         = $('.form-cd');
        let $book       = $('.form-book');
        let $furniture  = $('.form-furniture');

        switch ( $value ) {
            case 'cd':
                $cd
                    .show()
                    .find('input, textarea')
                    .prop('disabled', false);
                $book
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                $furniture
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                break;
            case 'book':
                $cd
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                $book
                    .show()
                    .find('input, textarea')
                    .prop('disabled', false);
                $furniture
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                break;
            case 'furniture':
                $cd
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                $book
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                $furniture
                    .show()
                    .find('input, textarea')
                    .prop('disabled', false);
                break;
            default:
                $cd
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                $book
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                $furniture
                    .hide()
                    .find('input')
                    .prop('disabled', true);
                break;
        }
    });

    //Function allows 'Apply' button to redirect or submit form on main page.
    $('#apply').click(function () {
        let selected = parseInt($('.main-select').val());
        if (selected === 1) {
            redirect();
        }
        if (selected === 2) {
            submitForm();
        }
    });
})