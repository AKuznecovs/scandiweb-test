<?php

namespace Core;

use Helpers\Helpers;
use Helpers\Session;
use Models\ProductRepository;

/**
 * Class Controller has basic app controllers methods.
 */
class Controller
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Helpers
     */
    protected $helpers;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view         = new View();
        $this->helpers      = new Helpers();
        $this->session      = new Session();
        $this->repository   = new ProductRepository();
    }

    /**
     * Logs out user, closes user's session. Redirects to the Main page.
     * @param  string   $button     button's name
     * @return void
     */
    public function logOut($button, $page)
    {
        if (isset($_POST[$button])) {
            $this->session->closeSession();
            $this->helpers->redirect($page);
        }
    }
}
