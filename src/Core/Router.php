<?php

namespace Core;

/**
 * Class Router. Basic app routing.
 */
class Router
{

    /**
     * Method has basic routing rules. Accepts url queries, passes them as params to action.
     * In case of wrong paths- redirects to error page.
     */
    public function process()
    {

        $params             = [];
        $controller_name    = DEFAULT_CONTROLLER;
        $action_name        = DEFAULT_CONTROLLER_ACTION;

        $url = parse_url($_SERVER['REQUEST_URI']);

        // Path: controller + action
        $path = explode("/", ltrim($url['path'], "/"));

        if (empty($path[0])) {
            $this->redirect('main');
        }

        // Parsing query string into array of params ( params['name'] => "value" )
        if (!empty($url['query'])) {
            parse_str($url['query'], $params);
        }

        $controller_name = ucfirst($path[0]) . 'Controller';
        $controller_path = CONTROLLER_PATH . $controller_name . '.php';

        if (!empty($path[0]) && !file_exists($controller_path)) {
            $this->redirect('error');
        }

        if (!empty($path[1])) {
            $action_name = ucfirst($path[1]) . 'Action';
        }

        $controller_class = 'Controllers\\' . $controller_name;
        $controller = new $controller_class();

        if (file_exists($controller_path) && method_exists($controller, $action_name)) {
            $controller->$action_name($params);
        } else {
            $this->redirect('error');
        }
    }

    /**
     * Redirects to the Error page, stops all current scripts.
     */
    public function redirect($page)
    {
        header('Location: /' . $page);
        return;
    }
}
