<?php

namespace Core;

/**
 * Class View renders templates, passes data from controller to template.
 */
class View
{
    /**
     * Defines default template
     * @var string
     */
    public $default = 'template_view';

    /**
     * Generates view based on content.
     * @param $content_view
     * @param string $default file with default HTML markup template
     * @param array $data data passed from the Controllers
     * @return false|string
     */
    public function generate($content_view, $default, $data = [])
    {
        $template_path = TEMPLATE_VIEW_PATH . $default;

        if (is_array($data)) {
            extract($data);
        }
        require $template_path;
    }
}
