<?php

namespace Core;

use Database\Database;
use \PDO;

/**
 * Class Model. Basic app model, opens connection with database.
 */
class Model
{
    /**
     * Database
     * @var string
     */
    protected $db;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        /**
         * Defines $this->db as an object
         * @param string DB_HOST    global variable for server
         * @param string DB_USER    global variable for users
         * @param string DB_PASS    global variable for password_verify
         * @param string DB_NAME    global variable for database
         */
        $this->db = new Database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }
}
