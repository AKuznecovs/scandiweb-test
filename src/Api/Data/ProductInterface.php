<?php declare(strict_types=1);

namespace Api\Data;

/**
 * Interface ProductInterface for product's class
 */
interface ProductInterface
{
    /**
     * Get id
     * @return int
     */
    public function getId(): int;

    /**
     * Get name
     * @return string|null
     */
    public function getName(): string;

    /**
     * Get price
     * @return float|null
     */
    public function getPrice(): ?float;

    /**
     * Get sku
     * @return string
     */
    public function getSku(): string;

    /**
     * Get value
     *
     * @return string|null
     */
    public function getValue(): ?string;
}
