<?php

namespace Models;

/**
 * Class Add, model class for the "add" page.
 */
class Add
{
    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * Add constructor.
     * @param \Models\ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Creates new product object
     * @param $params
     * @return mixed
     */
    public function createProductObject($params)
    {
        if (isset($_POST['addProduct']) || $_SERVER['REQUEST_METHOD'] == 'POST') {
            $filtered_params = array_filter($params, 'strlen');

            return $this->repository->create($filtered_params);
        }
    }

    /**
     * Adds product object to the database
     * @param $object
     */
    public function saveNewProduct($object)
    {
        $params = [
        'sku'       =>  $object->getSku(),
        'name'      =>  $object->getName(),
        'type'      =>  $object->getType(),
        'price'     =>  $object->getPrice(),
        'value'     =>  $object->getValue()
        ];

        $this->repository->save($params);
    }
}
