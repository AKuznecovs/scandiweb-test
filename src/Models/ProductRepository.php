<?php

namespace Models;

use \PDO;
use Core\Model;

/**
 * Class ProductRepository, connection with database and it's products.
 * @package Models
 */
class ProductRepository extends Model
{

    /**
     * Deletes product from DB using it's ID.
     * @param $id
     */
    public function deleteById($id)
    {
        try {
            $this->db->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "DELETE FROM `products` WHERE id = :id";
            $upstm = $this->db->conn->prepare($sql);
            $upstm->bindParam(':id', $id);
            $upstm->execute();
        } catch (\PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        header("Location: /main");
        return;
    }

    /**
     * Saves product and it's properties to the DB.
     * @param $props
     */
    public function save($props)
    {
        try {
            $this->db->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO `products` (sku, name, price, type, value)
                    VALUES (:sku, :name, :price, :type, :value)";
            $upstm = $this->db->conn->prepare($sql);
            $upstm->bindParam(':sku', $props['sku']);
            $upstm->bindParam(':name', $props['name']);
            $upstm->bindParam(':price', $props['price']);
            $upstm->bindParam(':type', $props['type']);
            $upstm->bindParam(':value', $props['value']);
            $upstm->execute();
        } catch (\PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        header("Location: /main");
        return;
    }

    /**
     * Creates new product's object based on it's type to interact with it's properties.
     * @param $params
     * @return mixed
     */
    public function create($params)
    {
        $className = 'Models\Product\\' . ucfirst($params['type']);
        $new_product = new $className($params);
        $new_product->setValue($params);

        return $new_product;
    }

    /**
     * Method fetches all data from database 'products' table.
     * @return array
     */
    public function getList()
    {
        $items = [];
        try {
            $this->db->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "SELECT * FROM `products`";
            $upstm = $this->db->conn->prepare($sql);
            $upstm->execute();
            $results = $upstm->fetchAll(PDO::FETCH_OBJ);
            foreach ($results as $object) {
                $product = self::initializeType($object);
                $items[] = $product;
            }
        } catch (\PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }


        return $items;
    }

    /**
     * Defines the product classname and type, to set it's value accordingly.
     * @param $object
     * @return mixed
     */
    public static function initializeType($object)
    {
        $className = '\Models\Product\\' . ucfirst($object->type);
        return new $className($object);
    }
}
