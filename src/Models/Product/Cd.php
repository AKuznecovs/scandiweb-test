<?php

namespace Models\Product;

/**
 * Class Cd, product's type with value size(MB).
 */
class Cd extends Product
{
    protected $type;
    protected $size;

    /**
     * Cd constructor.
     * @param $params
     */
    public function __construct($params)
    {
        parent::__construct($params);

        $this->type = Product::CD;
        $value      = $this->value;
        $this->size = $value;
    }

    /**
     * Gets product's type
     * @return string
     */
    public function getType()
    {
        return Product::CD;
    }

    /**
     * Gets product's size
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Sets value as size with "MB"
     * @return string
     */
    public function getFormattedValue()
    {
        return "Size: " . $this->getValue() . " MB";
    }

    /**
     * Value setter
     * @param $result
     * @return string
     */
    public function setValue($result)
    {
        $this->size   = $result['size'];
        $this->value  = $result['size'];
        return $this->value;
    }
}
