<?php

namespace Models\Product;

/**
 * Class Furniture, product's type with value height, length, width(cm).
 */
class Furniture extends Product
{
    protected $type;
    protected $height;
    protected $width;
    protected $length;

    /**
     * Furniture constructor.
     * @param $params
     */
    public function __construct($params)
    {
        parent::__construct($params);
        $this->type = Product::FURNITURE;
    }

    /**
     * Type getter
     * @return string
     */
    public function getType()
    {
        return Product::FURNITURE;
    }

    /**
     * Height getter
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Width getter
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Length getter
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Sets value as dimension
     * @return string
     */
    public function getFormattedValue()
    {
        return "Dimension: " . $this->getValue();
    }

    /**
     * Value setter
     * @param $result
     */
    public function setValue($result)
    {
        $this->height = $result['height'];
        $this->width  = $result['width'];
        $this->length = $result['length'];

        $dimensions   = array($result['height'], $result['width'], $result['length']);
        $this->value  = implode("x", $dimensions);
    }
}
