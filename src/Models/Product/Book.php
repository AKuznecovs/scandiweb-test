<?php


namespace Models\Product;

/**
 * Class Book, product's type with value weight(kg).
 */
class Book extends Product
{
    protected $type;
    protected $weight;

    /**
     * Book constructor.
     * @param $params
     */
    public function __construct($params)
    {
        parent::__construct($params);

        $this->type   = Product::BOOK;
        $value        = $this->value;
        $this->weight = $value;
    }

    /**
     * Type getter
     * @return string
     */
    public function getType()
    {
        return Product::BOOK;
    }

    /**
     * Weight getter
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Sets value as string with weight.
     * @return string
     */
    public function getFormattedValue()
    {
        return "Weight: " . $this->getValue() . " KG";
    }

    /**
     * Value setter
     * @param $result
     */
    public function setValue($result)
    {
        $this->value  = $result['weight'];
        $this->weight = $result['weight'];
    }
}
