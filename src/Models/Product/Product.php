<?php

namespace Models\Product;

use Api\Data\ProductInterface;

/**
 * Class Product, main class for product objects, contains basic methods.
 * @package Models\Product
 */
class Product implements ProductInterface
{
    const CD        = 'cd';
    const BOOK      = 'book';
    const FURNITURE = 'furniture';

    protected $id;
    protected $sku;
    protected $name;
    protected $price;
    protected $value;

    /**
     * Product constructor.
     * @param $params
     */
    public function __construct($params)
    {
        foreach ($params as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Get id
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get price
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * Get formatted price
     * @return string
     */
    public function getFormattedPrice(): string
    {
        return number_format((float)$this->getPrice(), 2, '.', '') . " $";
    }

    /**
     * Get sku
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * Get value
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }
}
