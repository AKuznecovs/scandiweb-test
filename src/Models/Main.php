<?php

namespace Models;

/**
 * Class Main, model class for the "main" page.
 */
class Main
{

    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * Main constructor.
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Displays all products
     * @return array
     */
    public function getProductList()
    {
        return $this->repository->getList();
    }

    /**
     * Deletes product entries, selected using checkboxes.
     */
    public function massDelete()
    {
        if (isset($_POST['check']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            foreach ($_POST['check'] as $id) {
                $this->repository->deleteById($id);
            }
        }
    }
}
