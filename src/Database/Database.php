<?php

namespace Database;

use \PDO;

/**
 * Class DB provides general connection with database and catches connection errors.
 */
class Database
{

    /**
     * @var PDO
     */
    public $conn;

    public function __construct($server, $user, $pw, $db)
    {
        try {
            $this->conn = new PDO("mysql:host=$server;dbname=$db", $user, $pw);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            echo "<p>Connection failed: </p>" . $e->getMessage();
        }
    }
}
