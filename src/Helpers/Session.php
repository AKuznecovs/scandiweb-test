<?php

namespace Helpers;

/**
 * Class Session for basic session methods.
 */
class Session
{
    /**
     * Adds values to specific session.
     * @param $name
     * @param $value
     * @return int
     */
    public function addSessionValue($name, $value)
    {

        return $_SESSION[$name] = strlen($value);
    }

    /**
     * Getter for specific session.
     * @param $name
     * @return mixed|null
     */
    public function getSessionValue($name)
    {

        return !empty($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    /**
     * Closes session, destroys session values.
     */
    public function closeSession()
    {

        session_register_shutdown();
        session_unset();
        session_destroy();
    }

    /**
     * Removes specific session's value.
     * @param $name
     */
    public function removeSessionValue($name)
    {

        if (!empty($_SESSION[$name])) {
            unset($_SESSION[$name]);
        }
    }

    /**
     * Checks current user session. Redirects to the Main page
     * if user is not recognized. If no errors - display welcome message.
     * @return  bool    bool
     */
    public function checkSession()
    {
        if (empty($_SESSION['login'])) {
            $this->removeSessionValue('username');
            return false;
        } else {
            return true;
        }
    }
}
