<?php

namespace Helpers;

/**
 * Class Validation. Validates and sanitizes user's inputs data, passes errors to controllers.
 */
class Validation
{
    /**
     * @var array
     */
    private $errors;

    /**
     * @var string[] Regex patterns array.
     */
    public static $regexExpressions = array(
        'number'      => '^\d+$',
        'price'       => '^\d{0,8}([.|,]\d{1,2})?$'
    );

    /**
     * Validation constructor.
     */
    public function __construct()
    {
        $this->errors   = array();
    }

    /**
     * Method checks the length of input data, must not be empty to validate.
     * @param $field
     * @return bool
     */
    public function checkLength($field)
    {
        if (empty($_POST[$field])) {
            return false;
        }
        return true;
    }

    /**
     * Number validation.
     * @param $field int
     * @return int|null
     */
    public function checkNumber($field)
    {
        $number = $this->sanitizeInput($field, 'int');

        if ($this->checkLength($field) === false) {
            $this->addError($field, 'Number is missing');
        } elseif (!is_numeric($number)) {
            $this->addError($field, 'Invalid number!');
        } elseif (!preg_match('!' . self::$regexExpressions['number'] . '!i', $number)) {
            $this->addError($field, 'Please use correct numbers only!');
        }

        return $number;
    }

    /**
     * Number validation. Allows 2 digits number.
     * @param $field float
     * @return int|null
     */
    public function checkFloat($field)
    {
        $integer = $this->sanitizeInput($field, 'float');

        if ($this->checkLength($field) === false) {
            $this->addError($field, 'Decimal number is missing!');
        } elseif (!is_numeric($integer)) {
            $this->addError($field, 'Invalid decimal number!');
        }
        if (!preg_match('!' . self::$regexExpressions['price'] . '!i', $integer)) {
            $this->addError($field, 'Please use numbers with 2 decimals only!');
        }

        return $integer;
    }

    /**
     * Basic text validation.
     * @param $field string
     * @return string|null
     */
    public function checkText($field)
    {
        $text = $this->sanitizeInput($field, 'string');

        if ($this->checkLength($field) === false) {
            $this->addError($field, 'Text is missing');
        }

        if (!is_string($text) || (filter_var($text, FILTER_SANITIZE_STRING) === false)) {
            $this->addError($field, 'Invalid text');
        }

        return $text;
    }

    /**
     * Product's type validation. Checks if no type selected
     * @param $field string
     * @return string|null
     */
    public function checkType($field)
    {
        $type = $_POST['type'];

        if ($this->checkLength($field) === false) {
            $this->addError($field, "Product's type was not selected");
        }

        return $type;
    }

    /**
     * Adds error message to array of errors.
     * @param $field
     * @param $errorMsg
     * @return void
     */
    public function addError($field, $errorMsg)
    {
        return $this->errors[$field] = $errorMsg;
    }

    /**
     * Shows array of errors.
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Basic input value sanitization.
     * @param $field
     * @param $type
     * @return string
     */
    public function sanitizeInput($field, string $type)
    {
        $input = $_POST[$field];
        $add = '';

        $input = trim($input);
        $input = stripslashes($input);
        $input = strip_tags($input);

        // case string = default
        switch ($type) {
            case 'int':
                $filter = FILTER_SANITIZE_NUMBER_INT;
                break;
            case 'float':
                $filter = FILTER_SANITIZE_NUMBER_FLOAT;
                $add =  FILTER_FLAG_ALLOW_FRACTION;
                break;
            case 'string':
            default:
                $filter = FILTER_SANITIZE_STRING;
                break;
        }

        $output = filter_var($input, $filter, $add);
        return htmlspecialchars($output, ENT_QUOTES);
    }
}
