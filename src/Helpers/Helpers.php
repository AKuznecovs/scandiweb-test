<?php

namespace Helpers;

use Helpers\Session;

/**
 * Class Helpers contains some helpers methods for controller classes.
 */
class Helpers
{

    /**
     * Redirects to the Main page.
     * @param string $url page's name
     */
    public function redirect($url)
    {
        header('Location: /' . $url);
        return;
    }
}
