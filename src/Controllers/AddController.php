<?php

namespace Controllers;

use Core\Controller;
use Helpers\Validation;
use Models\Add;
use Models\ProductRepository;

/**
 * Class Add for Add product page.
 */
class AddController extends Controller
{

    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * @var Add
     */
    protected $model;

    /**
     * @var Validation
     */
    protected $validation;

    /** @var array */
    protected $params = [];

    /** @var array */
    protected $errors = [];

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model        = new Add($this->repository);
        $this->validation   = new Validation();
    }

    /**
     * Index action for Add page.
     * Validates and sanitizes inputs data, generates view according templates.
     */
    public function indexAction()
    {
        $params = $this->validateInputs();

        $data = [
          'errors' => $this->validation->getErrors()
        ];

        $this->view->generate('addpage_view.phtml', 'template_view.phtml', $data);
        $this->create($params);
    }

    /**
     * Validates inputs data, returns sanitized data,
     * @return array
     */
    public function validateInputs()
    {
        if (isset($_POST['addProduct']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
            return array(
                'sku'    => $this->validation->checkText('sku'),
                'name'   => $this->validation->checkText('name'),
                'price'  => $this->validation->checkFloat('price'),
                'type'   => $this->validation->checkType('type'),
                'size'   => array_key_exists('size', $_POST) ? $this->validation->checkNumber('size') : '',
                'weight' => array_key_exists('weight', $_POST) ? $this->validation->checkFloat('weight') : '',
                'height' => array_key_exists('height', $_POST) ? $this->validation->checkNumber('height') : '',
                'width'  => array_key_exists('width', $_POST) ? $this->validation->checkNumber('width') : '',
                'length' => array_key_exists('length', $_POST) ? $this->validation->checkNumber('length') : ''
            );
        }
    }

    /**
     * Creates product object with params if data matches the validation criteria,
     * if errors- stops product's adding process and displays errors.
     * @param $params
     */
    public function create($params)
    {
        if (!empty($this->validation->getErrors()) || empty($_POST['type'])) {
            exit();
        } else {
            $product = $this->model->createProductObject($params);
            $this->model->saveNewProduct($product);
        }
    }
}
