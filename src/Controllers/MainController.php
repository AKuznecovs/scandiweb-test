<?php

namespace Controllers;

use Core\Controller;
use Models\Main;
use Models\ProductRepository;

/**
 * Class MainController for main page.
 */
class MainController extends Controller
{

    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * @var Main
     */
    protected $model;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new Main($this->repository);
    }

    /**
     * Index action for main page.
     * Lists all products according type, generates view based on templates and
     * watches for server's request if "mass delete" selected.
     */
    public function indexAction()
    {
        $products = $this->model->getProductList();

        $data = [
            'products' => $products
        ];

        $this->view->generate('mainpage_view.phtml', 'template_view.phtml', $data);

        $this->model->massDelete();
    }
}
