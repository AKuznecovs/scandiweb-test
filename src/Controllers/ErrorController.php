<?php

namespace Controllers;

use Core\Controller;

/**
 * Class ErrorController for Error page.
 */
class ErrorController extends Controller
{
    /**
     * ErrorController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index action for error page.
     * Clears all session data, generates view acccorging the templates.
     */
    public function indexAction()
    {
        $this->logOut('back', 'main');

        $this->view->generate('errorpage_view.phtml', 'template_view.phtml');
    }
}
