let gulp = require('gulp');
let sass = require('gulp-sass');
let uglifycss = require('gulp-uglifycss');

sass.compiler = require('node-sass');

// Sass Task
gulp.task('sass', function () {
    return gulp.src('public/assets/styles/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(uglifycss({
            "uglyComments": true
        }))
        .pipe(gulp.dest('public/assets/styles/css/'));
});

gulp.task('watch', function () {
    gulp.watch('public/assets/styles/**/*.scss', gulp.series('sass'));
});