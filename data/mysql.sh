#!/bin/bash
sudo -v

BACKTICK='`'
PROJECT='swtest'
USER='admin'
DATABASE='scandiweb_test'

MYSQL=`which mysql`
PASS='Option123#'

Q1="CREATE DATABASE IF NOT EXISTS $BACKTICK$DATABASE$BACKTICK;"
Q2="GRANT ALL ON *.* TO '$USER'@'localhost' IDENTIFIED BY '$PASS';"
Q3="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}"

$MYSQL -uroot -proot -e "$SQL"

echo "MySQL user $USER created with password $PASS and assigned to database $DATABASE";
cat > /var/www/scandiweb-test/data/sql/credentials.txt << EOF

User: $USER
Pass: $PASS
Database: $DATABASE
EOF

$MYSQL -u "$USER" -p "$DATABASE" < /var/www/scandiweb-test/data/sql/sql_dump.sql

if [[ $? -ne 0 ]]; then         #(-ne) not equal command
  echo ""
  echo "Wrong password. Please re-enter password for $USER"
  $MYSQL -u "$USER" -p "$DATABASE" < /var/www/scandiweb-test/data/sql/sql_dump.sql
fi
