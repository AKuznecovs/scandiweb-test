#!/bin/bash
sudo -v

PROJECT_NAME="swtest"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ -z "$1" ]; then
	echo "Available vhost projectname type: $PROJECT_NAME"
	exit 1
fi

if [ -f /etc/apache2/sites-available/$1.conf ]; then
	echo "ERROR: Project already exists, remove it first"
	exit 1
fi

echo "Creating a 'SW test' vhost for $1 with a webroot in /var/www/scandiweb-test/public"
sudo cp $DIR/apache2/sites-available/$1.conf /etc/apache2/sites-available/$1.conf

if [ $? -ne 0 ]; then
	echo "Failed to create Apache2 configuration."
	exit 1
fi

# adding entry in /etc/hosts
sudo sed -i '1s/^/127.0.0.1		'$1'.local\n/' /etc/hosts

sudo a2ensite $1.conf
sudo a2enmod rewrite

sudo sudo service apache2 reload

echo "Done! http://$1.local ready!"