#!/bin/bash
sudo -v

PROJECT_NAME="swtest"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
 
if [ -z "$1" ]; then
	echo "Available vhost projectname type: $PROJECT_NAME"
	exit 1
fi
 
if [ -f /etc/nginx/sites-available/$1 ]; then
	echo "ERROR: Project already exists, remove it first"
	exit 1
fi

echo "Creating a 'SW test' vhost for $1 with a webroot in /var/www/scandiweb-test/public"
sudo cp $DIR/nginx/sites-available/$1 /etc/nginx/sites-available/$1

# adding entry in /etc/hosts
sudo sed -i '1s/^/127.0.0.1		'$1'.local\n/' /etc/hosts

# linking project's nginx conf sites-available with sites-enabled
sudo ln -s /etc/nginx/sites-available/$1 /etc/nginx/sites-enabled/$1

sudo service nginx reload
 
echo "Done! http://$1.local ready!"
